const ProductController = require('./../controllers/product.controller')

module.exports = function (app) {
    app.get('/products/:aisle/:bin', (req, res) => {
        ProductController.getByBinAndAisle(req, res);
    })

    app.get('/products/attributes/', (req, res) => {
        ProductController.getAllAttributes(req, res);
    })

    app.get('/products/:image_id', (req, res) => {
        ProductController.getByImage(req, res);
    })

    app.get('/products/', (req, res) => {
        ProductController.get(req, res);
    })


    app.post('/products/', (req, res) => {
        ProductController.insert(req, res);
    })

    app.put('/products/:_id', (req, res) => {
        ProductController.update(req, res);
    })

    app.delete('/products/:_id', (req, res) => {
        ProductController.delete(req, res);
    })


}