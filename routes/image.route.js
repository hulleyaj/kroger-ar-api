const ImagesController = require('../controllers/image.controller')

module.exports = function (app) {
    app.get('/image/:image_id', (req, res) => {
        console.log(req.params);
        ImagesController.getById(req, res);
    })
}