const Product = require('../models/image.model');

//=============================
// Get
//=============================
exports.getById = function (req, res) {
  Product.find({ image_id: req.params.image_id }).exec()
    .then((image) => {
      return res.json(image);
    })
    .catch((err) => {
      return next(err);
    });
};



