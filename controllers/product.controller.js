const Product = require('../models/product.model');
const Image = require('../models/image.model');

const productWidth = 0.35;
const productHeight = 0.5;
const aisleWidth = 2;

//=============================
// Gets
//=============================
exports.get = function (req, res) {
  var query = {};

  query = req.query.attributes ? { ... query, attributes: req.query.attributes } : query;
  query = req.query.aisle ? { ... query, aisle: req.query.aisle } : query;
  query = req.query.bin ? { ... query, bin: req.query.bin } : query;
  query = req.query.name ? { ... query, name: {"$regex": req.query.name, "$options": "i" }} : query;

  Product.find(query).exec()
    .then((products) => {
      products.forEach(product => product = translatePosToCoor(product));
      return res.json(products);
    })
    .catch((err) => {
      return next(err);
    });
};

exports.getAllAttributes = function (req, res) {
  Product.distinct('attributes').exec()
    .then((attributes) => {
      return res.json(attributes);
    })
    .catch((err) => {
      return next(err);
    });
};

exports.getByBinAndAisle = function (req, res) {
  var query = {
    aisle: req.params.aisle,
    bin: req.params.bin
  };

  query = req.query.attributes ? { ...query, attributes: req.query.attributes } : query;

  Product.find(query).exec()
    .then((products) => {
      products.forEach(product => product = translatePosToCoor(product));
      return res.json(products);
    })
    .catch((err) => {
      return next(err);
    });
};

exports.getByImage = function (req, res) {
  var query = {
    image_id: req.params.image_id
  };

  Image.findOne(query).exec()
    .then((image) => {

      var productQuery = {
        aisle: image.aisle,
        bin: image.bin,
      };
      
      productQuery = req.query.attributes ? { ... productQuery, attributes: req.query.attributes } : productQuery;
      productQuery = req.query.name ? { ... productQuery, name: req.query.name } : productQuery;

    
      Product.find(productQuery).exec()
        .then((products) => {
          
          products.forEach(product => product = translatePosToCoor(product));
          return res.json(products);
        })
        .catch((err) => {
          return err;
        });
    })
    .catch((err) => {
      return err;
    });

  
};


//=============================
// Post
//=============================
exports.insert = (req, res) => {
  const product = new Product(req.body);
  console.log(product);
  product.save()
  .then(() => {
    return res.send('Successfully added');
  })
  .catch((err) => {
    return next(err);
  });
}

//=============================
// Update
//=============================
exports.update = (req, res) => {
  const newProduct = new Product(req.body);
  console.log(newProduct);
  Product.updateOne({_id: req.params._id}, { 
    $set: {
      "name": newProduct.name
    }
  })
  .then(() => {
    return res.send('Successfully added');
  })
  .catch((err) => {
    return res.send(err);
  });
}


//=============================
// Delete
//=============================
exports.delete = (req, res) => {
  Product.remove({_id: req.params._id})
  .then(() => {
    return res.send('Successfully removed');
  })
  .catch((err) => {
    return res.send(err);
  });
}



translatePosToCoor = function(product)
{
  product.coor.x = (product.location.x - 2) * productWidth;
  product.coor.y = product.location.y * productHeight + 0.2;
  product.coor.z = -1;
  return product;
}


