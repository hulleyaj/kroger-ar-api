const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const MongoClient = require('mongodb').MongoClient
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/kroger-ar', function(err) {
    if (err) {
        console.err(err);
    } else {
        console.log('Connected');
  app.listen(process.env.PORT || 3000, () => {
    console.log('listening on 3000')
  })
    }    
});

app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(express.static('public'))
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH");
    next();
  });

require('./routes/product.route')(app);
require('./routes/image.route')(app);
