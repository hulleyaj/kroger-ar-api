const mongoose = require('mongoose');

//=============================
// Schema
//=============================
var ProductSchema = new mongoose.Schema({
  name: String,
  description: String,
  aisle: Number,
  image_id: Number,
  bin: Number,
  attributes: [String],
  location: {
    x: Number,
    y: Number,
    z: Number,
  },
  coor: {
    x: Number,
    y: Number,
    z: Number,
  },
},
  {
    versionKey: false // You should be aware of the outcome after set to false
});

const ModelClass = mongoose.model('product', ProductSchema);

module.exports = ModelClass;
