const mongoose = require('mongoose');

//=============================
// Schema
//=============================
var ImageSchema = new mongoose.Schema({
  image_id: Number,
  bin: Number,
  aisle: Number,
});

const ModelClass = mongoose.model('image', ImageSchema);

module.exports = ModelClass;
